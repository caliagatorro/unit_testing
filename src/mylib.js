/** Basic arithmetic operations */
const mylib = {
    /** Multiline arrow function
    * @param {number} a
    * @param {number} b
    * @returns {number}
    */
    add: (a, b) => {
        const sum = a + b;
        return sum;
    },

    /** Multiline arrow function
    * @param {number} a
    * @param {nubmer} b
    * @returns {number}
    */
    sub: (a, b) => {
        return a - b;
    },

    /** Singleline arrow function
    * @param {number} dividend 
    * @param {number} divisor 
    * @returns {number}
    */
    divide: (dividend, divisor) => {
        if (divisor === 0) {
            throw new Error("Divisor 0 not allowed.");
        }
        return dividend / divisor;
    },

    /** Regular function
    * @param {number} a
    * @param {number} b
    * @returns {number}
    */
    multiply: function (a, b) {
        return a * b;
    },
};

module.exports = mylib;
