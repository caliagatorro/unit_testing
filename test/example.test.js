const expect = require('chai').expect;
const mylib = require('../src/mylib');

describe("My unit tests", () => {
    it("Adds 2 + 2 and equals 4", () => {
        expect(mylib.add(2,2)).to.equal(4)
    });
    it("Fails if divisor is 0", () => {
        // expect something to throw an error.
        expect(() => mylib.divide(9,0)).to.throw();
    });
    after(() => {
        // do something after the testing
    });
    it("Substract 4 - 2 and equals 2", () => {
        expect(mylib.sub(4,2)).equal(2)
    });
    it("Multiply 4 * 2 and equals 8", () => {
        expect(mylib.multiply(4,2)).equal(8)
    });
});
